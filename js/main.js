let red1 = Math.random() * 255;
let green1 = Math.random() * 255;
let blue1 = Math.random() * 255;
let red2 = Math.random() * 255;
let green2 = Math.random() * 255;
let blue2 = Math.random() * 255;

function animateBackgroundColor() {
    red1 = red1 + (Math.random() * 10 - 5);
    green1 = green1 + (Math.random() * 10 - 5);
    blue1 = blue1 + (Math.random() * 10 - 5);
    red2 = red2 + (Math.random() * 10 - 5);
    green2 = green2 + (Math.random() * 10 - 5);
    blue2 = blue2 + (Math.random() * 10 - 5);
    document.body.style.background = "linear-gradient(to right, rgb(" + red1 + "," + green1 + "," + blue1 + "), rgb(" + red2 + "," + green2 + "," + blue2 + "))";
    setTimeout(animateBackgroundColor, 10);
}

function animateTitle() {
    let title = document.getElementById("title");
    let titleText = title.innerHTML;
    title.innerHTML = titleText.substring(1, titleText.length) + titleText.charAt(0);
    setTimeout(animateTitle, 200);
}

window.addEventListener('DOMContentLoaded', () => {
    animateBackgroundColor();
    animateTitle();
});